import os
import sys
from collections import OrderedDict

IFO = os.getenv('IFO')
if not IFO:
    sys.exit('Must specify IFO env var.')


def ifochans(channels):
    return ['{}:{}'.format(IFO, chan) for chan in channels]


LOG_FMT = '%(asctime)s [%(module)s:%(funcName)s] %(message)s'
LOG_FMT_NOTIME = '[%(module)s:%(funcName)s] %(message)s'

EVENT_ROOT = os.getenv('LOCKLOST_EVENT_ROOT', '')
if os.path.exists(EVENT_ROOT):
    EVENT_ROOT = os.path.abspath(EVENT_ROOT)
SUMMARY_ROOT = os.getenv('LOCKLOST_SUMMARY_ROOT', '')
WEB_ROOT = os.getenv('LOCKLOST_WEB_ROOT', '')

SEG_DIR = os.path.join(EVENT_ROOT, '.segments')

CONDOR_ONLINE_DIR = os.path.join(EVENT_ROOT, '.condor_online')
CONDOR_SEARCH_DIR = os.path.join(EVENT_ROOT, '.condor_search')
CONDOR_ANALYZE_DIR = os.path.join(EVENT_ROOT, '.condor_analyze')

ONLINE_STAT_FILE = os.path.join(EVENT_ROOT, '.online-stat')

QUERY_DEFAULT_LIMIT = 50

CONDOR_ACCOUNTING_GROUP = os.getenv('CONDOR_ACCOUNTING_GROUP')
CONDOR_ACCOUNTING_GROUP_USER = os.getenv('CONDOR_ACCOUNTING_GROUP_USER')
CONDOR_KILL_SIGNAL = 'SIGTERM'

GRD_NODE = 'ISC_LOCK'
GRD_STATE_N_CHANNEL = '{}:GRD-{}_STATE_N'.format(IFO, GRD_NODE)
GRD_STATE_S_CHANNEL = '{}:GRD-{}_STATE_S'.format(IFO, GRD_NODE)

CA_MONITOR_CHANNEL = GRD_STATE_S_CHANNEL

if IFO == 'H1':
    GRD_NOMINAL_STATE = (600, 'NOMINAL_LOW_NOISE')
elif IFO == 'L1':
    GRD_NOMINAL_STATE = (2000, 'LOW_NOISE')
GRD_LOCKLOSS_STATES = [(2, 'LOCKLOSS')]

if IFO == 'H1':
    CDS_EVENT_ROOT = 'https://lhocds.ligo-wa.caltech.edu/exports/lockloss/events'
elif IFO == 'L1':
    CDS_EVENT_ROOT = 'https://llocds.ligo-la.caltech.edu/data/lockloss/events'

O4_GPS_START = 1368975618

SEARCH_STRIDE = 10000

DATA_ACCESS = os.getenv('DATA_ACCESS', 'gwpy')

DATA_DEVSHM_ROOT = os.path.join("/dev/shm/lldetchar/", IFO)
DATA_DEVSHM_TIMEOUT = 80

DATA_DISCOVERY_SLEEP = 30
DATA_DISCOVERY_TIMEOUT = int(os.getenv('DATA_DISCOVERY_TIMEOUT', 1200))

# tag colors for web (foreground, background)
TAG_COLORS = {
    'OBSERVE': ('black', 'lime'),
    'REFINED': ('white', 'grey'),
    'BRS_GLITCH': ('rebeccapurple', 'lavender'),
    'BOARD_SAT': ('navy', 'cornflowerblue'),
    'WINDY': ('springgreen', 'mediumblue'),
    'EARTHQUAKE': ('orange', 'black'),
    'MICROSEISMIC': ('yellow', 'black'),
    'ANTHROPOGENIC': ('red', 'black'),
    'ADS_EXCURSION': ('mediumvioletred', 'plum'),
    'FSS_OSCILLATION': ('69650b', 'palegoldenrod'),
    'ETM_GLITCH': ('navy', 'red'),
    'ISS': ('beige', 'darkolivegreen'),
    'SEI_BS_TRANS': ('sienna', 'orange'),
    'COMMISSIONING': ('lightseagreen', 'darkslategrey'),
    'MAINTENANCE': ('lightseagreen', 'darkslategrey'),
    'CALIBRATION': ('lightseagreen', 'darkslategrey'),
    'FAST_DRMI': ('blue', 'pink'),
    'SOFT_LIMITERS': ('hotpink', 'green'),
    'OMC_DCPD': ('#dcd0ff', '#734f96'),
    'INITIAL_ALIGNMENT': ('yellow', 'purple'),
    'PI_MONITOR': ('seashell', 'coral'),
    'VIOLIN': ('linen', 'saddlebrown'),
    'IMC': ('coral', 'palegoldenrod'),
}

##################################################

PLOT_WINDOWS = {
    'WIDE': [-30, 10],
    'ZOOM': [-5, 1],
}

FIG_SIZE = [n*0.6 for n in [16, 9]]

##################################################

REFINE_WINDOW = [-20, 5]
REFINE_PLOT_WINDOWS = {
    'WIDE': REFINE_WINDOW,
    'ZOOM': [-5, 1],
}

# per IFO key is the state index threshold for the specified indicator
# params, e.g. for H1, for lock losses from states between 102 and 410
# the indicator parameters will be...
# CHANNEL: indicator channel
# THRESHOLD: % stddev relative to mean (e.g. "-10" is "less than 10*stdev below mean")
# MINIMUM: minimum mean value for which indicator is valid
if IFO == 'H1':
    INDICATORS = {
        410: {
            'CHANNEL': 'H1:ASC-AS_A_DC_NSUM_OUT_DQ',
            'THRESHOLD': -25,
            'MINIMUM': 50,
        },
        102: {
            'CHANNEL': 'H1:LSC-POPAIR_B_RF18_I_ERR_DQ',
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
        0: {
            'CHANNEL': 'H1:IMC-TRANS_OUT_DQ',
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
    }
elif IFO == 'L1':
    INDICATORS = {
        0: {
            'CHANNEL': 'L1:IMC-TRANS_OUT_DQ',
            'THRESHOLD': -100,
            'MINIMUM': 50,
        },
    }

##################################################

PLOT_SATURATIONS = 8
PLOT_CHUNKSIZE = 3000

# This function should give the saturation threshold for a given channel
# It checks if a channel is a known 20, 18, or 16-bit daq
# If it is not a 20 or 16-bit daq is says it is a 18-bit daq
# Will default to 18-bit daq


def get_saturation_threshold(chan, gps, IFO):
    # Threshold is divided by two for the counts can be positive or negative
    SATURATION_THRESHOLD_16 = (2**16) / 2
    SATURATION_THRESHOLD_18 = (2**18) / 2
    SATURATION_THRESHOLD_20 = (2**20) / 2
    SATURATION_THRESHOLD_28 = (2**28) / 2
    # Initializing bit channel lists so we don't run into issues when returning
    TWENTYEIGHT_BIT_CHANNELS = []
    TWENTY_BIT_CHANNELS = []
    EIGHTEEN_BIT_CHANNELS = []
    SIXTEEN_BIT_CHANNELS = [
        'SUS-RM',
        'SUS-ZM',
        'SUS-OM1',
        'SUS-OM2',
        'SUS-OM3'
    ]

    if IFO == 'L1':
        # This corresponds to a change from 18 to 20-bit DAC for ETMY L3 channels.
        # ETMY L3 channels after this date have counts that are four times higher
        CHANGE_DAC_DATE = 1188518418  # 2017/09/04
        # This gets the DAC configuration in line with https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=63133
        # Exact DAC times should be added later to better analyze old events
        CHANGE_DAC_DATE2 = 1358033599  # 2023/01/17
        # https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=63808
        CHANGE_DAC_DATE3 = 1361911279  # 2023/03/03
    elif IFO == 'H1':
        # This corresponds to a change from 18 to 20-bit DAC for ETMX L3 channels.
        # ETMX L3 channels after this date have counts that are four times higher
        CHANGE_DAC_DATE = 1224961218  # 2018/10/30
        # This gets the DAC configuration in line with https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=66836
        # Exact DAC times should be added later to better analyze old events
        CHANGE_DAC_DATE2 = 1358024479  # 2023/01/17
        # Change from 20-bit to 28-bit DACs on H1 EX L1, L2, L3 only
        # See https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=80167
        CHANGE_DAC_DATE3 = 1410642901  # 2024/09/18
    else:
        # Assuming missed channel is 18-bit
        return SATURATION_THRESHOLD_18

    # Before CHANGE_DAC_DATE (L1 2017/09/04, H1 2018/10/30)
    if gps < CHANGE_DAC_DATE:
        if any(x in chan for x in SIXTEEN_BIT_CHANNELS):
            return SATURATION_THRESHOLD_16
        else:
            return SATURATION_THRESHOLD_18
    # Between CHANGE_DAC_DATE and CHANGE_DAC_DATE2
    # (L1 2017/09/04-2023/01/17, H1 2018/10/30-2023/01/17)
    elif gps >= CHANGE_DAC_DATE and gps < CHANGE_DAC_DATE2:
        # ETM's are 20-bit and were changed at a date within our wanted
        # lockloss time range
        if IFO == 'L1':
            ETM_L3_CHANNELS = 'SUS-ETMY_L3'
        elif IFO == 'H1':
            ETM_L3_CHANNELS = 'SUS-ETMX_L3'
        if ETM_L3_CHANNELS in chan:
            return SATURATION_THRESHOLD_20
        elif any(x in chan for x in SIXTEEN_BIT_CHANNELS):
            return SATURATION_THRESHOLD_16
        else:
            return SATURATION_THRESHOLD_18
    # Between CHANGE_DAC_DATE2 and CHANGE_DAC_DATE3
    # (L1 2023/01/17-2023/03/03, H1 2023/01/17-2024/09/18)
    elif gps >= CHANGE_DAC_DATE2:
        if IFO == 'L1':
            TWENTY_BIT_CHANNELS = [
                'SUS-ETM',
                'SUS-ITM',
                'SUS-BS',
                'SUS-MC1',
                'SUS-MC2',
                'SUS-MC3',
                'SUS-PRM',
                'SUS-PR2',
                'SUS-PR3',
                'SUS-SR2',
                'SUS-SRM_M2',
                'SUS-SRM_M3'
            ]
            EIGHTEEN_BIT_CHANNELS = [
                'SUS-SRM_M1',
                'SUS-SR3',
                'SUS-OMC',
                'SUS-IM'
            ]
        elif IFO == 'H1':
            TWENTY_BIT_CHANNELS = [
                'SUS-ETMX_L1',
                'SUS-ETMX_L2',
                'SUS-ETMX_L3',
                'SUS-ETMY_L1',
                'SUS-ETMY_L2',
                'SUS-ETMY_L3',
                'SUS-ITMX_L2',
                'SUS-ITMX_L3',
                'SUS-ITMY_L2',
                'SUS-ITMY_L3',
                'SUS-BS_M2',
                'SUS-SRM_M2',
                'SUS-SRM_M3',
                'SUS-SR2_M2',
                'SUS-SR2_M3'
            ]
            EIGHTEEN_BIT_CHANNELS = [
                'SUS-ETMX_M0',
                'SUS-ETMY_M0',
                'SUS-ITMX_M0',
                'SUS-ITMX_L1',
                'SUS-ITMY_M0',
                'SUS-ITMY_L1',
                'SUS-MC1',
                'SUS-MC2',
                'SUS-MC3',
                'SUS-PRM',
                'SUS-PR2',
                'SUS-PR3',
                'SUS-SRM_M1',
                'SUS-SR2_M1',
                'SUS-SR3',
                'SUS-BS_M1',
                'SUS-OMC',
            ]
        # After CHANGE_DAC_DATE3 (L1 2023/03/03, H1 2024/09/18)
        if gps >= CHANGE_DAC_DATE3:
            if IFO == 'L1':
                TWENTY_BIT_CHANNELS = TWENTY_BIT_CHANNELS + EIGHTEEN_BIT_CHANNELS
            elif IFO == 'H1':
                TWENTYEIGHT_BIT_CHANNELS = [
                    'SUS-ETMX_L1',
                    'SUS-ETMX_L2',
                    'SUS-ETMX_L3'
                ]
                for new_susstage in TWENTYEIGHT_BIT_CHANNELS:
                    for susstage in TWENTY_BIT_CHANNELS:
                        if new_susstage == susstage:
                            TWENTY_BIT_CHANNELS.remove(susstage)

    # Return saturation thresholds
    if any(x in chan for x in TWENTYEIGHT_BIT_CHANNELS):
        return SATURATION_THRESHOLD_28
    elif any(x in chan for x in TWENTY_BIT_CHANNELS):
        return SATURATION_THRESHOLD_20
    elif any(x in chan for x in EIGHTEEN_BIT_CHANNELS):
        return SATURATION_THRESHOLD_18
    elif any(x in chan for x in SIXTEEN_BIT_CHANNELS):
        return SATURATION_THRESHOLD_16
    else:
        # Assuming missed channel is 18-bit
        return SATURATION_THRESHOLD_18


ANALOG_BOARD_CHANNELS = [
    'IMC-REFL_SERVO_SPLITMON',
    'IMC-REFL_SERVO_FASTMON',
    'IMC-REFL_SERVO_SUMMON',
    'LSC-REFL_SERVO_SUMMON',
    'LSC-REFL_SERVO_SPLITMON',
    'LSC-REFL_SERVO_FASTMON',
    'LSC-REFL_SERVO_SLOWFBMON',
    'LSC-REFL_SERVO_SLOWMON',
]
ANALOG_BOARD_CHANNELS = ifochans(ANALOG_BOARD_CHANNELS)

BOARD_SEARCH_WINDOW = [-30, 5]

if IFO == 'H1':
    BOARD_SAT_BUFFER = 0
if IFO == 'L1':
    BOARD_SAT_BUFFER = 0.5

BOARD_SAT_THRESH = 9.5

##################################################

if IFO == 'H1':
    ADS_GRD_STATE = (429, 'PREP_ASC_FOR_FULL_IFO')
elif IFO == 'L1':
    ADS_GRD_STATE = (1197, 'START_SPOT_CENTERING')

ADS_CHANNELS = []
for py in ['PIT', 'YAW']:
    for num in [3, 4, 5]:
        ADS_CHANNELS.append('%s:ASC-ADS_%s%s_DEMOD_I_OUT16' % (IFO, py, num))

ADS_SEARCH_WINDOW = [-40, 5]

ADS_THRESH = 0.1

##################################################

SOFT_SEARCH_WINDOW = [-20, 0]

ASC_INMON = []
for asc in ['CHARD', 'CSOFT', 'DHARD', 'DSOFT', 'SRC1', 'SRC2', 'PRC1', 'PRC2', 'INP1', 'INP2', 'MICH']:
    for dof in ['P', 'Y']:
        ASC_INMON.append(f'{IFO}:ASC-{asc}_{dof}_SMOOTH_INMON')

ASC_LIMIT = []
for asc in ['CHARD', 'CSOFT', 'DHARD', 'DSOFT', 'SRC1', 'SRC2', 'PRC1', 'PRC2', 'INP1', 'INP2', 'MICH']:
    for dof in ['P', 'Y']:
        ASC_LIMIT.append(f'{IFO}:ASC-{asc}_{dof}_SMOOTH_LIMIT')

ASC_ENABLE = []
for asc in ['CHARD', 'CSOFT', 'DHARD', 'DSOFT', 'SRC1', 'SRC2', 'PRC1', 'PRC2', 'INP1', 'INP2', 'MICH']:
    for dof in ['P', 'Y']:
        ASC_ENABLE.append(f'{IFO}:ASC-{asc}_{dof}_SMOOTH_ENABLE')

ASC = [j for i in [ASC_INMON, ASC_LIMIT, ASC_ENABLE] for j in i]

##################################################

OMC_DCPD_CHANNELS = [
    'FEC-8_ADC_OVERFLOW_0_12',
    'FEC-8_ADC_OVERFLOW_0_13',
]
OMC_DCPD_CHANNELS = ifochans(OMC_DCPD_CHANNELS)

OMC_DCPD_WINDOW = [-5, 1]

OMC_DCPD_BUFFER = 1

##################################################

if IFO == 'H1':
    INIT_GRD_STATE = (307, 'SHUTTER_ALS')

ITM_CHANNELS = []  # pulls both ITM error channel signals
for arm in ['X', 'Y']:
    for dof in ['PIT', 'YAW']:
        ITM_CHANNELS.append('%s:ALS-%s_CAM_ITM_%s_ERR_OUT16' % (IFO, arm, dof))

ALS_WFS_DOF1 = []  # channels for ALS DOF1 WFS
for arm in ['X', 'Y']:
    for dof in ['P', 'Y']:
        ALS_WFS_DOF1.append('%s:ALS-%s_WFS_DOF_1_%s_OUT16' % (IFO, arm, dof))

ALS_WFS_DOF2 = []  # channels for ALS DOF2 WFS
for arm in ['X', 'Y']:
    for dof in ['P', 'Y']:
        ALS_WFS_DOF2.append('%s:ALS-%s_WFS_DOF_2_%s_OUT16' % (IFO, arm, dof))

ALS_WFS_DOF3 = []  # channels for ALS DOF3 WFS
for arm in ['X', 'Y']:
    for dof in ['P', 'Y']:
        ALS_WFS_DOF3.append('%s:ALS-%s_WFS_DOF_3_%s_OUT16' % (IFO, arm, dof))

DOF_1_SCALE = 0.0004  # scaled values for all DOF WFS
DOF_2_SCALE = 10**-7
DOF_3_SCALE = 0.001

WFS_THRESH = 0.8

INITIAL_ALIGNMENT_SEARCH_WINDOW = [-20, 0]

INITIAL_ALIGNMENT_THRESH = 0.15

##################################################

LPY_CHANNELS = [
    'SUS-ETMX_L2_MASTER_OUT_UR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_UL_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LL_DQ',
]
LPY_CHANNELS = ifochans(LPY_CHANNELS)

##################################################

BOARD_SAT_CM = [
    'mediumseagreen',
    'cornflowerblue',
    'mediumvioletred',
    'orangered',
    'darkorchid',
    'goldenrod',
    'teal',
    'lightcoral',
]
SATURATION_CM = [
    '#332288',
    '#88CCEE',
    '#117733',
    '#999933',
    '#DDCC77',
    '#CC6677',
    '#882255',
    '#AA4499',
]
SC_CM = [
    '#e6194b',
    '#3cb44b',
    '#4363d8',
    '#f58231',
    '#911eb4',
    '#46f0f0',
    '#f032e6',
    '#bcf60c',
    '#008080',
]

ADC_OVERFLOWS = {
    'ASC': {
        'ADC_ID': 19,
        'num_bits': 5,
        'bit_exclude': [],
    },
    'LSC': {
        'ADC_ID': 10,
        'num_bits': 3,
        'bit_exclude': [(2, 4), (2, 13), (2, 15)],
    },
}
if IFO == 'H1':
    ADC_OVERFLOWS['OMC'] = {
        'ADC_ID': 8,
        'num_bits': 1,
        'bit_exclude': [(2, 4), (2, 13), (2, 15)],
    }

SAT_SEARCH_WINDOW = [-30, 5]

##################################################

WIND_SEARCH_WINDOW = [-600, 5]
if IFO == 'H1':
    WIND_CHANNELS = [
        'H1:PEM-EX_WIND_ROOF_WEATHER_MPH',
        'H1:PEM-EY_WIND_ROOF_WEATHER_MPH',
        'H1:PEM-CS_WIND_ROOF_WEATHER_MPH',
    ]
    WIND_THRESH = 20
elif IFO == 'L1':
    WIND_CHANNELS = [
        'L1:PEM-EY_WIND_WEATHER_MPH',
        'L1:PEM-LVEA_WIND_WEATHER_MPH',
    ]
    WIND_THRESH = 20

##################################################

# Set seismic band thresholds based on IFO site
if IFO == 'H1':
    SEI_EARTHQUAKE_THRESH = 300
    SEI_ANTHROPOGENIC_THRESH = 1000  # placeholder
    SEI_MICROSEISMIC_THRESH = 3000   # placeholder
if IFO == 'L1':
    SEI_EARTHQUAKE_THRESH = 600
    SEI_ANTHROPOGENIC_THRESH = 1000
    SEI_MICROSEISMIC_THRESH = 3000

# Main data structure, each subdictionary contains the channel, save
# file name, data quality key, channel's data axis, threshold for the
# channel, and tag designation
SEISMIC_CONFIG = {
    'EQ band': {
        'channel': 'ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON',
        'savefile': 'seismic_eq.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_EARTHQUAKE_THRESH,
        'tag': 'EARTHQUAKE',
    },
    'anthropogenic corner': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ITMY.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'anthropogenic Xend': {
        'channel': 'ISI-GND_STS_ETMX_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ETMX.png',
        'dq_channel': 'ISI-GND_STS_ETMX_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'anthropogenic Yend': {
        'channel': 'ISI-GND_STS_ETMY_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ETMY.png',
        'dq_channel': 'ISI-GND_STS_ETMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'micro ITMY Y 100u 300u': {
        'channel': 'ISI-GND_STS_ITMY_Y_BLRMS_100M_300M',
        'savefile': 'seismic_micro_Y_100u_300u.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Y_DQ',
        'axis': 'Y',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Y 300u 1': {
        'channel': 'ISI-GND_STS_ITMY_Y_BLRMS_300M_1',
        'savefile': 'seismic_micro_Y_300u_1.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Y_DQ',
        'axis': 'Y',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Z 100u 300u': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_100M_300M',
        'savefile': 'seismic_micro_Z_100u_300u.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Z 300u 1': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_300M_1',
        'savefile': 'seismic_micro_Z_300u_1.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
}

# Loop to convert channel names to the IFO channel names.  Could not
# use ifochans function since ifochans was only pulling one character
# at a time from channel string
for i in SEISMIC_CONFIG:
    SEISMIC_CONFIG[i]['channel'] = '{}:{}'.format(IFO, SEISMIC_CONFIG[i]['channel'])
    SEISMIC_CONFIG[i]['dq_channel'] = '{}:{}'.format(IFO, SEISMIC_CONFIG[i]['dq_channel'])

SEI_SEARCH_WINDOW = [-30, 150]

##################################################

GSTLAL_TRIGGER_DIR = '/home/idq/gstlal/online/features'

GLITCH_PLOT_WINDOWS = {
    'WIDE': [-20, 2],
    'ZOOM': [-5, 1],
}

GLITCH_CHANNELS = {
    'ASC_AS': [
        'ASC-AS_A_DC_NSUM_OUT_DQ',
        'ASC-AS_A_DC_PIT_OUT_DQ',
        'ASC-AS_A_DC_YAW_OUT_DQ',
        'ASC-AS_A_RF36_I_PIT_OUT_DQ',
        'ASC-AS_A_RF36_I_YAW_OUT_DQ',
        'ASC-AS_A_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_A_RF45_I_PIT_OUT_DQ',
        'ASC-AS_A_RF45_I_YAW_OUT_DQ',
        'ASC-AS_A_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF45_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF36_I_PIT_OUT_DQ',
        'ASC-AS_B_RF36_I_YAW_OUT_DQ',
        'ASC-AS_B_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF45_I_PIT_OUT_DQ',
        'ASC-AS_B_RF45_I_YAW_OUT_DQ',
        'ASC-AS_B_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF45_Q_YAW_OUT_DQ',
    ],
    'ASC_HS': [
        'ASC-CHARD_Y_OUT_DQ',
        'ASC-CSOFT_P_OUT_DQ',
        'ASC-CSOFT_Y_OUT_DQ',
        'ASC-DHARD_P_OUT_DQ',
        'ASC-DHARD_Y_OUT_DQ',
        'ASC-DSOFT_P_OUT_DQ',
        'ASC-DSOFT_Y_OUT_DQ',
        'ASC-MICH_P_OUT_DQ',
        'ASC-MICH_Y_OUT_DQ',
        'ASC-PRC1_Y_OUT_DQ',
        'ASC-PRC2_P_OUT_DQ',
    ],
    'ASC_REFL': [
        'ASC-REFL_A_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_A_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_A_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_YAW_OUT_DQ',
        'ASC-REFL_B_DC_NSUM_OUT_DQ',
        'ASC-REFL_B_DC_PIT_OUT_DQ',
        'ASC-REFL_B_DC_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_B_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_YAW_OUT_DQ',
        'ASC-SRC2_P_OUT_DQ',
        'ASC-SRC2_Y_OUT_DQ',
    ],
    'ASC_TR': [
        'ASC-X_TR_A_NSUM_OUT_DQ',
        'ASC-X_TR_A_PIT_OUT_DQ',
        'ASC-X_TR_A_YAW_OUT_DQ',
        'ASC-X_TR_B_NSUM_OUT_DQ',
        'ASC-X_TR_B_PIT_OUT_DQ',
        'ASC-X_TR_B_YAW_OUT_DQ',
        'ASC-Y_TR_A_NSUM_OUT_DQ',
        'ASC-Y_TR_A_PIT_OUT_DQ',
        'ASC-Y_TR_A_YAW_OUT_DQ',
        'ASC-Y_TR_B_NSUM_OUT_DQ',
        'ASC-Y_TR_B_PIT_OUT_DQ',
        'ASC-Y_TR_B_YAW_OUT_DQ',
    ],
    'IMC': [
        'IMC-DOF_1_P_IN1_DQ',
        'IMC-DOF_2_P_IN1_DQ',
        'IMC-DOF_4_P_IN1_DQ',
        'IMC-F_OUT_DQ',
        'IMC-IM4_TRANS_PIT_OUT_DQ',
        'IMC-IM4_TRANS_SUM_IN1_DQ',
        'IMC-IM4_TRANS_YAW_OUT_DQ',
        'IMC-MC2_TRANS_PIT_OUT_DQ',
        'IMC-PWR_IN_OUT_DQ',
        'IMC-REFL_DC_OUT_DQ',
        'IMC-TRANS_OUT_DQ',
        'IMC-WFS_A_DC_SUM_OUT_DQ',
        'IMC-WFS_A_I_PIT_OUT_DQ',
        'IMC-WFS_A_I_YAW_OUT_DQ',
        'IMC-WFS_A_Q_PIT_OUT_DQ',
        'IMC-WFS_A_Q_YAW_OUT_DQ',
        'IMC-WFS_B_DC_PIT_OUT_DQ',
        'IMC-WFS_B_DC_YAW_OUT_DQ',
        'IMC-WFS_B_I_PIT_OUT_DQ',
        'IMC-WFS_B_Q_PIT_OUT_DQ',
        'IMC-WFS_B_Q_YAW_OUT_DQ',
    ],
    'LSC': [
        'LSC-MCL_IN1_DQ',
        'LSC-MICH_IN1_DQ',
        'LSC-MICH_OUT_DQ',
        'LSC-MOD_RF45_AM_AC_OUT_DQ',
        'LSC-POPAIR_A_RF9_Q_ERR_DQ',
        'LSC-POP_A_LF_OUT_DQ',
        'LSC-POP_A_RF45_I_ERR_DQ',
        'LSC-POP_A_RF9_Q_ERR_DQ',
        'LSC-PRCL_IN1_DQ',
        'LSC-PRCL_OUT_DQ',
        'LSC-REFL_A_LF_OUT_DQ',
        'LSC-REFL_A_RF45_I_ERR_DQ',
        'LSC-REFL_A_RF45_Q_ERR_DQ',
        'LSC-REFL_A_RF9_Q_ERR_DQ',
        'LSC-REFL_SERVO_ERR_OUT_DQ',
        'LSC-Y_ARM_OUT_DQ',
    ],
    'PSL': [
        'PSL-FSS_PC_MON_OUT_DQ',
        'PSL-ILS_HV_MON_OUT_DQ',
        'PSL-ILS_MIXER_OUT_DQ',
        'PSL-ISS_PDA_REL_OUT_DQ',
        'PSL-ISS_PDB_REL_OUT_DQ',
        'PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
        'PSL-OSC_PD_AMP_DC_OUT_DQ',
        'PSL-OSC_PD_INT_DC_OUT_DQ',
        'PSL-OSC_PD_ISO_DC_OUT_DQ',
        'PSL-PMC_HV_MON_OUT_DQ',
        'PSL-PMC_MIXER_OUT_DQ',
    ],
}

##################################################

LSC_ASC_CHANNELS = OrderedDict()
LSC_ASC_CHANNELS['Power Buildups'] = [
    'LSC-POPAIR_B_RF18_I_ERR_DQ',
    'LSC-POPAIR_B_RF90_I_ERR_DQ',
    'LSC-REFL_A_LF_OUT_DQ',
    'LSC-POP_A_LF_OUT_DQ',
    # 'ASC-X_PWR_CIRC_OUT16',
    # 'ASC-Y_PWR_CIRC_OUT16',
]
LSC_ASC_CHANNELS['LSC Control Signals'] = [
    'LSC-MICH_OUT_DQ',
    'LSC-PRCL_OUT_DQ',
    'LSC-SRCL_OUT_DQ',
    'LSC-DARM_OUT_DQ',
    # 'LSC-MICHFF_OUT_DQ',
    # 'LSC-SRCLFF1_OUT_DQ',
    'LSC-MCL_OUT_DQ',
    'LSC-REFL_SERVO_CTRL_OUT_DQ',
    'IMC-L_OUT_DQ',
    'IMC-F_OUT_DQ',
]
LSC_ASC_CHANNELS['LSC Sensors'] = [
    'LSC-POP_A_RF9_I_ERR_DQ',
    'LSC-POP_A_RF9_Q_ERR_DQ',
    'LSC-REFL_A_RF9_I_ERR_DQ',
    'LSC-REFL_A_RF9_Q_ERR_DQ',
    'LSC-POP_A_RF45_I_ERR_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Arm)'] = [
    'ASC-CHARD_Y_OUT_DQ',
    'ASC-CHARD_P_OUT_DQ',
    'ASC-DHARD_Y_OUT_DQ',
    'ASC-DHARD_P_OUT_DQ',
    'ASC-CSOFT_Y_OUT_DQ',
    'ASC-CSOFT_P_OUT_DQ',
    'ASC-DSOFT_Y_OUT_DQ',
    'ASC-DSOFT_P_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Vertex)'] = [
    'ASC-PRC1_Y_OUT_DQ',
    'ASC-PRC1_P_OUT_DQ',
    'ASC-PRC2_Y_OUT_DQ',
    'ASC-PRC2_P_OUT_DQ',
    'ASC-MICH_Y_OUT_DQ',
    'ASC-MICH_P_OUT_DQ',
    'ASC-SRC1_Y_OUT_DQ',
    'ASC-SRC1_P_OUT_DQ',
    'ASC-SRC2_Y_OUT_DQ',
    'ASC-SRC2_P_OUT_DQ',
    'ASC-INP1_Y_OUT_DQ',
    'ASC-INP1_P_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Centering Control Signals'] = [
    'ASC-DC1_Y_OUT_DQ',
    'ASC-DC1_P_OUT_DQ',
    'ASC-DC2_Y_OUT_DQ',
    'ASC-DC2_P_OUT_DQ',
    'ASC-DC3_Y_OUT_DQ',
    'ASC-DC3_P_OUT_DQ',
    'ASC-DC4_Y_OUT_DQ',
    'ASC-DC4_P_OUT_DQ',
]
for key in LSC_ASC_CHANNELS:
    LSC_ASC_CHANNELS[key] = ifochans(LSC_ASC_CHANNELS[key])


##################################################

if IFO == 'H1':
    PI_DICT = {
        f'{IFO}:OMC-PI_DOWNCONV_DC2_SIG_OUT_DQ': '80.0 kHz (PI28, PI29)',
        f'{IFO}:OMC-PI_DOWNCONV_DC6_SIG_OUT_DQ': '14.5 kHz (PI15, PI16)',
        f'{IFO}:OMC-PI_DOWNCONV_DC7_SIG_OUT_DQ': '10.0 kHz (PI24, PI31)',
    }
    PI_SAT_THRESH = 5e-3
    PI_YLABEL = 'Downconverted Signal [mA]'  # BP, shift down, then LP
    PI_YLIMS = [-1.5e-2, 1.5e-2]

if IFO == 'L1':
    PI_DICT = {
        f'{IFO}:OMC-BLRMS_32_BAND1_LOG10_OUTMON': '2.5 - 8 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND2_LOG10_OUTMON': '8 - 12 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND3_LOG10_OUTMON': '12 - 16 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND4_LOG10_OUTMON': '16 - 20 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND5_LOG10_OUTMON': '20 - 24 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND6_LOG10_OUTMON': '24 - 28 kHz',
        f'{IFO}:OMC-BLRMS_32_BAND7_LOG10_OUTMON': '28 - 32 kHz',
    }
    PI_SAT_THRESH = 0.5
    PI_YLABEL = 'Band-Limited RMS [log]'
    PI_YLIMS = [0, 1]

PI_CHANNELS = []
for chan in PI_DICT:
    PI_CHANNELS.append(chan)

PI_SEARCH_WINDOW = [-(8*60), 5]

##################################################

VIOLIN_CHANNELS = [
    'SUS-ITMX_L2_DAMP_MODE3_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE4_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE5_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE3_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE4_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE5_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE6_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE3_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE4_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE5_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE6_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE3_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE4_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE5_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE6_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE11_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE12_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE13_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE14_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE11_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE12_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE13_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE14_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE11_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE12_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE13_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE14_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE11_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE12_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE13_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE14_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE21_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE22_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE23_RMSLP_LOG10_OUTMON',
    'SUS-ITMX_L2_DAMP_MODE24_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE21_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE22_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE23_RMSLP_LOG10_OUTMON',
    'SUS-ITMY_L2_DAMP_MODE24_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE21_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE22_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE23_RMSLP_LOG10_OUTMON',
    'SUS-ETMX_L2_DAMP_MODE24_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE21_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE22_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE23_RMSLP_LOG10_OUTMON',
    'SUS-ETMY_L2_DAMP_MODE24_RMSLP_LOG10_OUTMON',
]

VIOLIN_CHANNELS = ifochans(VIOLIN_CHANNELS)

VIOLIN_SAT_THRESH = -15.5
VIOLIN_SEARCH_WINDOW = [-(5*60), 5]
