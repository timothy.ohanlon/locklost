import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from gwpy.segments import Segment
from gwpy.timeseries import TimeSeries

from .. import logger
from .. import config
from .. import data
from .. import plotutils


ASD_CHANNEL = [
    f'{config.IFO}:OMC-PI_DCPD_64KHZ_AHF_DQ'
]
ASD_WINDOW = [-(8*60), 0]


##############################################


def check_pi(event):
    """Checks OMC downconverted signals 2, 6, 7 (H1) or
    BAND 1-7 log channels (L1) for PIs.

    Checks if PI band goes above threshold and
    adds tag if above a certain threshold.

    """
    plotutils.set_rcparams()

    mod_window = [config.PI_SEARCH_WINDOW[0], config.PI_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    PI_channels = data.fetch(config.PI_CHANNELS, segment)

    mod_window_asd = [ASD_WINDOW[0], ASD_WINDOW[1]]
    segment_asd = Segment(mod_window_asd).shift(int(event.gps))
    ASD_data = data.fetch(ASD_CHANNEL, segment_asd)[0]
    asd_timeseries = TimeSeries(
        ASD_data.data,
        t0=segment_asd[0],
        dt=1/ASD_data.sample_rate
    )
    time_frame = 33 * 2
    fft_length = 16
    overlap = fft_length / 2
    before_pi = asd_timeseries.times.value - event.gps < ASD_WINDOW[0] + time_frame
    during_pi = asd_timeseries.times.value - event.gps > -time_frame
    asd_before = asd_timeseries[before_pi].asd(fftlength=fft_length, overlap=overlap)
    asd_during = asd_timeseries[during_pi].asd(fftlength=fft_length, overlap=overlap)

    saturating = False
    for buf in PI_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        idxs = ((t-event.gps) < -2) & ((t-event.gps) > -20)
        if np.max(buf.data[idxs]) > config.PI_SAT_THRESH:
            saturating = True

    if saturating:
        if config.IFO == 'H1':
            if event.transition_index[0] >= 600:
                logger.info('PI found')
                event.add_tag('PI_MONITOR')
        if config.IFO == 'L1':
            if event.transition_index[0] >= 1300:
                event.add_tag('PI_MONITOR')
    else:
        logger.info('no PI')

    gs = gridspec.GridSpec(2, 4)
    gs.update(wspace=0.5)
    fig = plt.figure(figsize=(22*3, 16*3))
    ax1 = fig.add_subplot(gs[0, :2])
    ax2 = fig.add_subplot(gs[0, 2:])
    ax3 = fig.add_subplot(gs[1, :])
    for idx, buf in enumerate(PI_channels):
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        # Plot all OMC PI channels on top two plots
        for ax in [ax1, ax2]:
            ax.plot(
                t-event.gps,
                buf.data,
                label=f'{buf.channel[3:]}: {config.PI_DICT[buf.channel]}',
                alpha=0.8,
                lw=2,
            )
    # Configuring top two plots
    for ax in [ax1, ax2]:
        # Add dotted thresholds to the plots
        ax.axhline(
            config.PI_SAT_THRESH,
            linestyle='--',
            color='black',
            label='PI threshold',
            lw=5,
        )
        ax.axhline(
            -config.PI_SAT_THRESH,
            linestyle='--',
            color='black',
            lw=5,
        )
        ax.set_xlabel(f'Time [s] since lock loss at {event.gps}', labelpad=10)
        ax.set_ylabel(config.PI_YLABEL)
        ax.set_ylim(config.PI_YLIMS[0], config.PI_YLIMS[1])
        ax.legend(loc='best')
        ax.set_title('PI BLRMS Monitors')
        ax.grid()

    ax1.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax2.set_xlim(-20, 5)

    ax3.plot(
        asd_during.frequencies / 1000,
        asd_during,
        linewidth=1,
        color='red',
        label='Right before lockloss'
    )
    ax3.plot(
        asd_before.frequencies / 1000, asd_before,
        linewidth=1,
        color='lightsteelblue',
        label='8 min before lockloss'
    )
    ax3.set_yscale('log')
    ax3.set_xscale('log')
    ax3.set_xlim(5, 32)
    ax3.set_title(f'ASD of {ASD_data.channel}')
    ax3.set_ylabel('Magnitude')
    ax3.set_xlabel('Frequency [kHz]')
    ax3.legend(loc='best')
    ax3.set_xticks(
        np.arange(5, 32),
        labels=['' if x % 5 != 0 else x for x in np.arange(5, 32)]
    )
    ax3.grid()

    outfile_plot = 'PI_monitor.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
