import numpy as np
import matplotlib.pyplot as plt

from gwpy.timeseries import TimeSeries

from .. import logger
from .. import config
from .. import plotutils


DARM_CHANNEL = f'{config.IFO}:GDS-CALIB_STRAIN_CLEAN'

##############################################


def plot_darm(event):
    """Grabs DARM data from 8 minutes before LL and right before
    LL and plots them
    """
    # Only create DARM plot if we got a LL from NLN or above
    if event.transition_index[0] < config.GRD_NOMINAL_STATE[0]:
        logger.info('IFO not fully locked, DARM plot will not be created.')
        return

    plotutils.set_rcparams()
    plt.rcParams['figure.figsize'] = (66, 24)

    logger.info('Plotting DARM 8 minutes vs right before lockloss')

    # Before time: between 8 and 9 minutes before LL
    time_before = [int(event.gps) - (9*60), int(event.gps) - (8*60)]
    # Right before LL time: 61 seconds to 1 second before LL
    time_before_LL = [int(event.gps) - 61, int(event.gps) - 1]

    # Grab the data
    darm_before = TimeSeries.get(
        DARM_CHANNEL,
        time_before[0],
        time_before[1],
        frametype=f'{config.IFO}_HOFT_C00',
        nproc=8,
        verbose=True
    )
    darm_before_LL = TimeSeries.get(
        DARM_CHANNEL,
        time_before_LL[0],
        time_before_LL[1],
        frametype=f'{config.IFO}_HOFT_C00',
        nproc=8,
        verbose=True
    )

    # Calculate the Strain PSD
    fft = 16
    psd_before = darm_before.psd(fft, fft/2.).crop(10, 5000)
    psd_before_LL = darm_before_LL.psd(fft, fft/2.).crop(10, 5000)

    # Plot DARM comparison
    plt.loglog(
        np.sqrt(psd_before),
        linewidth=1,
        color='red',
        label='Right before lockloss'
    )
    plt.loglog(
        np.sqrt(psd_before_LL),
        linewidth=1,
        color='lightsteelblue',
        label='8 min before lockloss'
    )
    plt.xlim(10, 5000)
    plt.xlabel('Frequency [Hz]')
    plt.ylabel(u'ASD [1/\u221AHz]')
    plt.title(f'DARM - {DARM_CHANNEL}')
    plt.suptitle(
        'Note: not helpful if not in NLN 9 mins before lockloss',
        size=35
    )
    plt.grid(which='both')
    plt.legend()
    plt.tight_layout()

    outfile_plot = 'darm.png'
    outpath_plot = event.path(outfile_plot)
    plt.savefig(outpath_plot, bbox_inches='tight')
