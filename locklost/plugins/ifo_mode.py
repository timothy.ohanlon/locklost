from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data


WINDOW = [-5, 0]
OBSERVE_CHANNEL = ['{}:GRD-IFO_OK'.format(config.IFO)]
IFO_MODE_CHANNEL = ['{}:ODC-OBSERVATORY_MODE'.format(config.IFO)]

# The Observatory mode is set by operators/commissioners to indicate when the
# IFO is in a non-Observing state. While GRD-IFO_OK is an absolute check that
# we are in Observing, ODC-OBSERVATORY_MODE is not guaranteed to represent the
# state of the Observatory.


##############################################


def check_ifo_mode(event):
    """Checks if IFO Observatory mode is set to MAINTENANCE, CALIBRATION,
    or COMMISSIONING modes and creates appropriate tag. """

    mode_window = [WINDOW[0], WINDOW[1]]
    segment = Segment(mode_window).shift(int(event.gps))
    observe_channel = data.fetch(OBSERVE_CHANNEL, segment)[0]

    if bool(observe_channel.data[0]):
        event.add_tag('OBSERVE')
        return

    mode_channel = data.fetch(IFO_MODE_CHANNEL, segment)[0]

    # commissioners are noise hunting, experimenting with controls, creating alignment scripts, etc.
    if any(mode_channel.data == 40):
        event.add_tag('COMMISSIONING')
    # all activities related to calibration, including calibration injections
    elif any(mode_channel.data == 51):
        event.add_tag('CALIBRATION')
    # for Tuesday maintenance as well as hardware/software failures that must be fixed
    elif any(mode_channel.data == 53) or any(mode_channel.data == 52):
        event.add_tag('MAINTENANCE')
    else:
        logger.info('IFO mode was not in commissioning, calibration, or maintenance.')
