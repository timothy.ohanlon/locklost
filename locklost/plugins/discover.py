from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data


##################################################


def discover_data(event):
    """Discovers sources of data for other plugins.

    If data is not available will wait until there is up to a maximum
    time specified.

    """
    # use transition gps since it doesn't depend on refinement
    gps = event.transition_gps

    # use refine window to determine data query range
    segment = Segment(*config.REFINE_WINDOW).shift(int(gps))

    logger.info("querying for data in range: {} - {}".format(*segment))

    try:
        data.fetch([config.GRD_STATE_N_CHANNEL], segment)
    except RuntimeError:
        assert False, f"{config.DATA_DISCOVERY_TIMEOUT}s data discovery timeout reached"

    logger.info("data available!")
