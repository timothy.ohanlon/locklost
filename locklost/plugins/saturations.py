import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


#################################################


def find_saturations(event):
    """Plots saturating suspension channels before lockloss.

    Create plot/list of saturating suspension channels before lockloss
    time (and returns corresponding gps times for saturations).

    """
    plotutils.set_rcparams()

    plot_first = config.PLOT_SATURATIONS
    sat_search_window = config.SAT_SEARCH_WINDOW
    gps = event.gps

    channels = gen_channel_names()
    bufs, segment = fetch_data(channels, gps)

    # checks for saturations within the first half of the search window,
    # and extends the search window if there are any
    expand = True
    num_expand = 0
    shift = -0.5*sat_search_window[0]
    while expand is True:
        relative_datasets = []
        saturation_values = []
        for idx, buf in enumerate(bufs):
            channel_name = str(buf.channel)
            saturation_threshold = config.get_saturation_threshold(channel_name, gps, config.IFO)
            reduced_data = buf.data/(saturation_threshold)
            relative_datasets.append(reduced_data)
            saturation_values.append(saturation_threshold)
            srate = buf.sample_rate
            early_thresh = int(shift*srate)
            if any(abs(reduced_data[:early_thresh]) >= 0.9):
                problem_area = np.where(abs(reduced_data[:early_thresh]) >= 0.9)[0]
                logger.info('Early saturation detected at index: '+str(problem_area[0]))
                logger.info('Saturation from channel: '+str(channel_name))
                if num_expand >= 8:
                    expand = False
                else:
                    bufs, segment = fetch_data(channels, gps, move_left=(num_expand+1)*shift)
                    num_expand += 1
                    expand = True
                    break
            else:
                expand = False

    # check for any channels saturating before event time and log
    # channel names, saturations time, full time series, and full data
    # series

    saturations = []
    for idx, buf in enumerate(bufs):
        channel_name = str(buf.channel)
        srate = buf.sample_rate
        reduced_dataset = relative_datasets[idx]
        saturation_value = saturation_values[idx]
        t = np.arange(segment[0], segment[1], 1/srate)
        newtime_idx = max(np.nonzero(t <= gps)[0])
        sat_idxs = np.nonzero(abs(reduced_dataset[:newtime_idx]) >= 1)[0]
        if len(sat_idxs) > 0:
            saturations.append([
                channel_name,
                t[min(sat_idxs)],
                t,
                reduced_dataset,
                saturation_value
            ])

    if not saturations:
        logger.info("No saturated suspension channels before lockloss.")

    else:
        logger.info("Saturated SUS channels before lockloss: "+str(len(saturations)))

        saturations.sort(key=lambda x: x[1])
        first_sat = saturations[0][1] - gps
        rev_sat = list(reversed(saturations[0:plot_first]))

        zoomed_window = [first_sat-5, first_sat+1]
        if first_sat - (segment[0] - gps) > 40:
            wide_window = [first_sat - 40, segment[1] - gps]
        else:
            wide_window = [segment[0] - gps, segment[1] - gps]
        windows = [wide_window, zoomed_window]
        zoom_levels = ['WIDE', 'ZOOM']
        xmaxs = [gps, first_sat+gps]
        buffers = [5, -1]
        for zoom_level, window, xmax, buffer in zip(zoom_levels, windows, xmaxs, buffers):

            ymin, ymax = set_ylims(saturations, xmax, buffer)
            fig, ax = plt.subplots(1, figsize=(22, 16))
            ax.set_xlabel('Time [s] since lock loss at {}'.format(gps), labelpad=10)
            ax.set_ylabel('Fraction of saturation threshold')
            ax.set_xlim(*window)
            ax.set_ylim(ymin, ymax)
            ax.grid()
            for idx, val in enumerate(rev_sat):
                ax.plot(
                    val[2]-gps,
                    val[3],
                    color=config.SATURATION_CM[idx],
                    label=f'{distill_channel_name(val[0])}',
                    alpha=0.8,
                    lw=2,
                )
            handles, labels = ax.get_legend_handles_labels()

            ax.axvline(
                first_sat,
                color='black',
                linestyle='--',
                lw=3,
            )
            ax.annotate(
                'First Saturation: '+str(first_sat+gps),
                xy=(first_sat, ax.get_ylim()[1]),
                xycoords='data',
                xytext=(0, 2),
                textcoords='offset points',
                horizontalalignment='center',
                verticalalignment='bottom',
                bbox=dict(boxstyle="round", fc="w", ec="black", alpha=0.95)
            )

            # draw lines on plot for saturation thresholds, lock loss time
            ax.axhline(
                1,
                color='red',
                linestyle='--',
                lw=10
            )
            ax.axhline(
                -1,
                color='red',
                linestyle='--',
                lw=10
            )
            ax.axvline(
                0,
                color='black',
                linestyle='-',
                lw=2
            )

            # Add labels listing saturation values above top red dashed line
            chan_sats = []
            for idx, val in enumerate(saturations[0:8]):
                short_name = distill_channel_name(val[0])[:-3]
                sat_val = '{:.2e}'.format(val[4])
                name_sat = f'{short_name}: {sat_val}'
                for j in range(0, len(chan_sats)):
                    if short_name in chan_sats[j]:
                        break
                    if sat_val in chan_sats[j]:
                        sat_split = chan_sats[j].split(':')
                        sat_chan_val = f'{sat_split[0]}, {short_name}:{sat_split[1]}'
                        chan_sats[j] = sat_chan_val
                        break
                else:
                    chan_sats.append(name_sat)
            saturation_thresh = 'Saturation thresholds:'
            for sat_set in chan_sats:
                saturation_thresh = f'{saturation_thresh} {sat_set};'
            saturation_thresh = saturation_thresh[:-1]
            # Find x and y coords for saturation text label that are set
            #  proportionally wrt to the y=1 horizontal line and the plot limits
            xmin, xmax = ax.get_xlim()
            sat_text_x = ((xmax - xmin)*0.03) + xmin
            sat_text_y = ((ymax-ymin)*0.015) + 1
            plt.text(
                sat_text_x,
                sat_text_y,
                saturation_thresh,
                color='red',
                size=22
            )

            ax.legend(handles[::-1], labels[::-1], bbox_to_anchor=(1.1, 0.96), title='optic/stage/corner')
            fig.tight_layout(pad=0.05)
            ax.set_title('Saturating suspension channels (ordered by time of saturation)', y=1.04)

            # saves plot to lockloss directory
            outfile_plot = 'saturations_{}.png'.format(zoom_level)
            outpath_plot = event.path(outfile_plot)
            fig.savefig(outpath_plot, bbox_inches='tight')
            fig.clf()

        # saves saturating channel names/times to lockloss directory
        outfile_csv = 'saturations.csv'
        outpath_csv = event.path(outfile_csv)
        with open(outpath_csv, 'wt') as myfile:
            for i in saturations:
                myfile.write('%s %f\n' % (i[0], i[1]))


def set_ylims(saturations, xmax, buffer):
    """Calculate ylims for the section of SUS channels occuring before
    lock loss time.
    """
    ymaxs = []
    ymins = []
    for sat in saturations:
        t, y = sat[2], sat[3]
        scaling_times = np.where(t < xmax-buffer)[0]
        scaling_vals = y[scaling_times]
        ymins.append(min(scaling_vals))
        ymaxs.append(max(scaling_vals))

    if max(ymaxs) >= 0:
        ymax = 1.1*max(ymaxs)
    else:
        ymax = 0.9*max(ymaxs)
    if min(ymins) <= 0:
        ymin = 1.1*min(ymins)
    else:
        ymin = 0.9*min(ymins)

    if ymin > -1:
        ymin = -1.1
    if ymax < 1:
        ymax = 1.1

    if ymin < -3:
        ymin = -3
    if ymax > 3:
        ymax = 3

    return ymin, ymax


def distill_channel_name(channel):
    """Distill a SUS channel name to display only optic, stage, corner info.

    """
    prefix, name = channel.split('-', 1)
    optic, stage, _, _, corner, _ = name.split('_', 5)
    return '{} {} {}'.format(optic, stage, corner)


def gen_channel_names():
    """Generates channel names for suspension systems.

    Returns: channels = SUS channel names as list of strings

    """
    channel_pattern = "{IFO}:SUS-{OPTIC}_{STAGE}_MASTER_OUT_{CORNER}_DQ"

    corners = ['UL', 'UR', 'LL', 'LR']
    triples = ['MC1', 'MC2', 'MC3', 'PRM', 'PR2', 'PR3', 'SRM', 'SR2', 'SR3']
    sixteen_bit = ['ZM1', 'ZM2', 'RM1', 'RM2', 'OM1', 'OM2', 'OM3']
    triple_stages = ['M2', 'M3']  # Only the lower stages, top handled separately
    triple_top_corners = ['T1', 'T2', 'T3', 'LF', 'RT', 'SD']
    quads = ['ETMX', 'ETMY', 'ITMX', 'ITMY']
    quad_stages = ['L1', 'L2', 'L3']  # Only the lower stages, top handled separately
    quad_top_corners = ['F1', 'F2', 'F3', 'LF', 'RT', 'SD']
    channels = []
    shortener = {}

    def add_channel(optic, stage, corner):
        channel_name = channel_pattern.format(IFO=config.IFO, OPTIC=optic, STAGE=stage, CORNER=corner)
        channels.append(channel_name)
        shortener[channel_name] = "%s %s %s" % (optic, stage, corner)

    for optic in quads:
        for corner in quad_top_corners:
            add_channel(optic, 'M0', corner)
        for stage in quad_stages:
            for corner in corners:
                add_channel(optic, stage, corner)

    for optic in triples:
        for corner in triple_top_corners:
            add_channel(optic, 'M1', corner)
        for stage in triple_stages:
            for corner in corners:
                add_channel(optic, stage, corner)

    for optic in sixteen_bit:
        for corner in corners:
            add_channel(optic, 'M1', corner)

    for corner in ['F1', 'F2', 'F3', 'LF', 'RT', 'SD']:
        add_channel('BS', 'M1', corner)
    for corner in corners:
        add_channel('BS', 'M2', corner)
    for corner in triple_top_corners:
        add_channel('OMC', 'M1', corner)

    return channels


def fetch_data(channels, gps, move_left=0, move_right=0):
    mod_window = [config.SAT_SEARCH_WINDOW[0]-move_left, config.SAT_SEARCH_WINDOW[1]+move_right]
    segment = Segment(mod_window).shift(int(gps))
    bufs = data.fetch(channels, segment)
    return bufs, segment
