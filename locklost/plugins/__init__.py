'''locklost followup analysis plugins

The package consists of functions to be run during a locklost event
followup analysis.  Plugin functions should take a LocklossEvent as
argument.

Plugins are registerd with the register_plugin() function.  Plugins
are currently executed sequentially in a single process, so register
ordering is preserved as a poor-man's dependency tree.

FIXME: figure out better way to express dependencies (e.g. DAG)

'''
import collections

import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault
plt.rcParams.update(rcParamsDefault)


def register_plugin(func):
    PLUGINS.update([(func.__name__, func)])


PLUGINS = collections.OrderedDict()

from .discover import discover_data
register_plugin(discover_data)

from .refine import refine_time
register_plugin(refine_time)

from .ifo_mode import check_ifo_mode
register_plugin(check_ifo_mode)

from .initial_alignment import initial_alignment_check
register_plugin(initial_alignment_check)

from .saturations import find_saturations
register_plugin(find_saturations)

from .lpy import find_lpy
register_plugin(find_lpy)

from .pi import check_pi
register_plugin(check_pi)

from .violin import check_violin
register_plugin(check_violin)

from .fss_oscillation import check_fss
register_plugin(check_fss)

from .iss import check_iss
register_plugin(check_iss)

from .etm_glitch import check_glitch
register_plugin(check_glitch)

from .ham6_power import power_in_ham6
register_plugin(power_in_ham6)

from .brs import check_brs
register_plugin(check_brs)

from .board_sat import check_boards
register_plugin(check_boards)

from .wind import check_wind
register_plugin(check_wind)

from .overflows import find_overflows
register_plugin(find_overflows)

from .lsc_asc import plot_lsc_asc
register_plugin(plot_lsc_asc)

from .glitch import analyze_glitches
register_plugin(analyze_glitches)

from .darm import plot_darm
register_plugin(plot_darm)

# Check other possible tags to add
from .ads_excursion import check_ads
register_plugin(check_ads)

from .sei_bs_trans import check_sei_bs
register_plugin(check_sei_bs)

from .soft_limiters import check_soft_limiters
register_plugin(check_soft_limiters)

from .omc_dcpd import check_omc_dcpd
register_plugin(check_omc_dcpd)

# Add the following at the end because they need to wait for additional data
from .seismic import check_seismic
register_plugin(check_seismic)

from .history import find_previous_state
register_plugin(find_previous_state)