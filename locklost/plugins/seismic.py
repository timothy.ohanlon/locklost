import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


##############################################


def check_seismic(event):
    """Check for elevated ground motion.

    Check the output of H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON around the
    time of lockloss, plot the data, and create a tag if above a
    certain threshold.

    """
    mod_window = [config.SEI_SEARCH_WINDOW[0], config.SEI_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))

    # Create the channel list that is used to get the data
    channel_list = []
    for v in config.SEISMIC_CONFIG.values():
        channel_list.append(v['channel'])
        if v['dq_channel'] not in channel_list:
            channel_list.append(v['dq_channel'])

    # Fetch data using channel list into the dictionary channel_data
    channel_data = data.fetch(channel_list, segment, as_dict=True)

    plotutils.set_rcparams()

    # Main loop, use config.SEISMIC_CONFIG keys to call data from channel_data dictionary
    for band, params in config.SEISMIC_CONFIG.items():

        channel = params['channel']
        dq_channel = params['dq_channel']

        blrms_t = channel_data[channel].tarray
        raw_t = channel_data[dq_channel].tarray

        fig, ax = plt.subplots(1, figsize=(22, 16))
        ln1 = ax.plot(
            blrms_t-event.gps,
            channel_data[channel].data,
            label=channel_data[channel].channel,
            alpha=0.8,
            lw=2,
            color='indigo',
        )
        ax2 = ax.twinx()
        ln2 = ax2.plot(
            raw_t-event.gps,
            channel_data[dq_channel].data,
            label=channel_data[dq_channel].channel,
            alpha=0.6,
            lw=2,
            color='seagreen',
        )
        ln3 = ax.axhline(
            params['threshold'],
            linestyle='--',
            color='indigo',
            label='Seismic BLRMS Threshold',
            lw=5,
        )
        # setting left y-axis paramters
        ax.spines['left'].set_color('indigo')
        ax.yaxis.label.set_color('indigo')
        ax.tick_params(axis='y', colors='indigo')
        ax.set_ylabel('Estimated peak velocity, 30-100 mHz band [nm/s]')
        # ax.set_ylim(0, max(channel_data[channel].data)+1)

        # setting right y-axis parameters
        ax2.spines['right'].set_color('seagreen')
        ax2.yaxis.label.set_color('seagreen')
        ax2.tick_params(axis='y', colors='seagreen')
        ax2.set_ylabel('Raw Output Velocity [nm/s]')

        # setting general plot parameters
        lns = ln1+ln2+[ln3]
        labs = [lab.get_label() for lab in lns]
        ax.legend(lns, labs, loc='best')
        ax.set_xlabel(
            'Time [s] since lock loss at {}'.format(event.gps),
            labelpad=10,
        )
        plt.xlim(blrms_t[0]-event.gps, blrms_t[-1]-event.gps)
        ax.set_title('{}-axis seismic motion'.format(params['axis']))

        fig.tight_layout(pad=0.05)
        outfile_plot = params['savefile']
        outpath_plot = event.path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')

        if any(channel_data[channel].data > params['threshold']):
            if not event.has_tag(params['tag']):
                event.add_tag(params['tag'])

    if not event.has_tag('EARTHQUAKE'):
        logger.info('Earthquake ground motion below threshold')

    if not event.has_tag('ANTHROPOGENIC'):
        logger.info('anthropogenic ground motion below threshold')

    if not event.has_tag('MICROSEISMIC'):
        logger.info('microseismic ground motion below threshold')
